﻿Imports System
Imports System.Runtime.InteropServices

'This is a sealed class containing win32 native functions
Friend NotInheritable Class NativeMethods

    Private Const MAX_PATH As Int16 = 256
    ' Browsing for directory.
    Public Const BIF_RETURNONLYFSDIRS As Integer = &H1
    Public Const BIF_DONTGOBELOWDOMAIN As Integer = &H2
    Public Const BIF_STATUSTEXT As Integer = &H4
    Public Const BIF_RETURNFSANCESTORS As Integer = &H8
    Public Const BIF_EDITBOX As Integer = &H10
    Public Const BIF_VALIDATE As Integer = &H20
    Public Const BIF_NEWDIALOGSTYLE As Integer = &H40
    Public Const BIF_USENEWUI As Integer = BIF_NEWDIALOGSTYLE Or BIF_EDITBOX
    Public Const BIF_BROWSEINCLUDEURLS As Integer = &H80
    Public Const BIF_BROWSEFORCOMPUTER As Integer = &H1000
    Public Const BIF_BROWSEFORPRINTER As Integer = &H2000
    Public Const BIF_BROWSEINCLUDEFILES As Integer = &H4000
    Public Const BIF_SHAREABLE As Integer = &H8000




    Public Const SHGFI_ICON As Integer = &H100 ' get icon
    Public Const SHGFI_DISPLAYNAME As Integer = &H200  'get display name
    Public Const SHGFI_TYPENAME As Integer = &H400 ' get type name
    Public Const SHGFI_ATTRIBUTES As Integer = &H800 ' get attributes
    Public Const SHGFI_ICONLOCATION As Integer = &H1000 ' get icon location
    Public Const SHGFI_EXETYPE As Integer = &H2000 ' return exe type
    Public Const SHGFI_SYSICONINDEX As Integer = &H4000 ' get system icon index
    Public Const SHGFI_LINKOVERLAY As Integer = &H8000 ' put a link overlay on icon
    Public Const SHGFI_SELECTED As Integer = &H10000 ' show icon in selected state
    Public Const SHGFI_ATTR_SPECIFIED As Integer = &H20000 ' get only specified attributes
    Public Const SHGFI_LARGEICON As Integer = &H0 ' get large icon
    Public Const SHGFI_SMALLICON As Integer = &H1 ' get small icon
    Public Const SHGFI_OPENICON As Integer = &H2 ' get open icon
    Public Const SHGFI_SHELLICONSIZE As Integer = &H4 ' get shell size icon
    Public Const SHGFI_PIDL As Integer = &H8 ' pszPath is a pidl
    Public Const SHGFI_USEFILEATTRIBUTES As Integer = &H10 ' use passed dwFileAttribute
    Public Const SHGFI_ADDOVERLAYS As Integer = &H20 ' apply the appropriate overlays
    Public Const SHGFI_OVERLAYINDEX As Integer = &H40 ' Get the index of the overlay
    Public Const FILE_ATTRIBUTE_DIRECTORY As Integer = &H10
    Public Const FILE_ATTRIBUTE_NORMAL As Integer = &H80



    Public Const SW_SHOW As Integer = 5
    Public Const SEE_MASK_INVOKEIDLIST As Integer = &HC

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Unicode)>
    Public Structure SHELLEXECUTEINFO
        Public cbSize As Integer
        Public fMask As Integer
        Public hwnd As IntPtr
        <MarshalAs(UnmanagedType.BStr)> Public lpVerb As String
        <MarshalAs(UnmanagedType.BStr)> Public lpFile As String
        <MarshalAs(UnmanagedType.BStr)> Public lpParameters As String
        <MarshalAs(UnmanagedType.BStr)> Public lpDirectory As String
        Dim nShow As Integer
        Dim hInstApp As IntPtr
        Dim lpIDList As IntPtr
        <MarshalAs(UnmanagedType.BStr)> Public lpClass As String
        Public hkeyClass As IntPtr
        Public dwHotKey As Integer
        Public hIcon As IntPtr
        Public hProcess As IntPtr
    End Structure


    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Unicode)>
    Public Structure SHFILEINFO
        Public Const NAMESIZE As Integer = 80
        Public ReadOnly hIcon As IntPtr
        Public iIcon As Integer
        Public dwAttributes As Integer
        <MarshalAs(UnmanagedType.BStr, SizeConst:=MAX_PATH)>
        Public szDisplayName As String
        <MarshalAs(UnmanagedType.BStr, SizeConst:=NAMESIZE)>
        Public szTypeName As String

    End Structure 'SHFILEINFO

    Private Sub New()
    End Sub


    <DllImport("user32.dll", CharSet:=CharSet.Auto)>
    Friend Shared Function DestroyIcon(ByVal hIcon As IntPtr) As Boolean

    End Function

    <DllImport("user32.dll", CharSet:=CharSet.Auto)>
    Friend Shared Function GetDesktopWindow() As IntPtr

    End Function

    <DllImport("kernel32.dll", CharSet:=CharSet.Unicode)>
    Friend Shared Function _lopen(ByVal lpPathName As String, ByVal iReadWrite As Integer) As Integer

    End Function

    <DllImport("kernel32.dll", CharSet:=CharSet.Auto)>
    Friend Shared Function _lclose(ByVal hFile As Integer) As Integer
    End Function


    <DllImport("shell32.dll", CharSet:=CharSet.Unicode)>
    Friend Shared Function ShellExecuteEx(ByRef lpExecInfo As SHELLEXECUTEINFO) As Boolean

    End Function

    <DllImport("shell32.dll", CharSet:=CharSet.Unicode)>
    Friend Shared Function ShellExecute(ByVal hwnd As IntPtr, ByVal lpszOp As String, ByVal lpszFile As String, ByVal lpszParams As String, ByVal lpszDir As String, ByVal FsShowCmd As Integer) As IntPtr

    End Function

    <DllImport("shell32.dll", CharSet:=CharSet.Unicode)>
    Friend Shared Function SHGetFileInfo(ByVal pszPath As String, ByVal dwFileAttributes As Integer, ByRef psfi As SHFILEINFO, ByVal cbFileInfo As Integer, ByVal uFlags As Integer) As IntPtr

    End Function




End Class
