﻿Imports Customers_Classes

Public Class CustomerComparer
    Implements IEqualityComparer(Of clsCustomer)
    Public Sub New()
        MyBase.New()
    End Sub

    Public Function Equals(ByVal c1 As clsCustomer, ByVal c2 As clsCustomer) As Boolean Implements IEqualityComparer(Of clsCustomer).Equals
        Return c1.CustomerID = c2.CustomerID
    End Function

    Public Function GetHashCode(ByVal c As clsCustomer) As Integer Implements IEqualityComparer(Of clsCustomer).GetHashCode
        Return c.CustomerID
    End Function
End Class
