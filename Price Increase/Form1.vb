﻿Imports System.IO
Imports System.Security.Principal
Imports Customers_Classes
Imports Customers_Classes.Connection
Imports Microsoft.Office.Interop
Imports Microsoft.Office.Interop.Word

Public Class Form1

    Private _mobjCustomer As clsCustomer
    Private _mcm As CurrencyManager
    Private _malBronzeAnnual As ArrayList
    Private _malBronzeMonthly As ArrayList
    Private _malBronzeRental As ArrayList
    Private _malBronzeAnnual_New As ArrayList
    Private _malBronzeMonthly_New As ArrayList
    Private _malBronzeRental_New As ArrayList
    Private _malSilverAnnual As ArrayList
    Private _malSilverMonthly As ArrayList
    Private _malSilverRental As ArrayList
    Private _malSilverAnnual_New As ArrayList
    Private _malSilverMonthly_New As ArrayList
    Private _malSilverRental_New As ArrayList
    Private _malGoldAnnual As ArrayList
    Private _malGoldMonthly As ArrayList
    Private _malGoldRental As ArrayList
    Private _malGoldAnnual_New As ArrayList
    Private _malGoldMonthly_New As ArrayList
    Private _malGoldRental_New As ArrayList

    Private Enum enumPaymentType
        None = 0
        Annual = 1
        Monthly = 2
        Rental = 3
    End Enum
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _malBronzeAnnual = New ArrayList()
        _malBronzeMonthly = New ArrayList()
        _malBronzeRental = New ArrayList()
        _malBronzeAnnual_New = New ArrayList()
        _malBronzeMonthly_New = New ArrayList()
        _malBronzeRental_New = New ArrayList()
        _malSilverAnnual = New ArrayList()
        _malSilverMonthly = New ArrayList()
        _malSilverRental = New ArrayList()
        _malSilverAnnual_New = New ArrayList()
        _malSilverMonthly_New = New ArrayList()
        _malSilverRental_New = New ArrayList()
        _malGoldAnnual = New ArrayList()
        _malGoldMonthly = New ArrayList()
        _malGoldRental = New ArrayList()
        _malGoldAnnual_New = New ArrayList()
        _malGoldMonthly_New = New ArrayList()
        _malGoldRental_New = New ArrayList()
    End Sub


    Private Sub AddCustomerBindings()

        If (_mobjCustomer IsNot Nothing) Then
            txtCustomerName.Text = _mobjCustomer.CustomerName
            txtAddress.Text = String.Concat(_mobjCustomer.Address, Environment.NewLine, _mobjCustomer.Postcode)
            txtInvoiceAddress.Text = _mobjCustomer.InvoiceTo
            lblDateBecameCustomer.Text = _mobjCustomer.DateBecameCustomer
            cboContacts.Items.Clear()
            cboContacts.Text = ""


            Using colContacts As colContacts = New colContacts()
                colContacts.SelectAll(_mobjCustomer.CustomerID)
                For Each contact As clsContact In colContacts
                    If String.IsNullOrWhiteSpace(contact.JobRole) Then
                        cboContacts.Items.Add(contact.ContactName)
                    Else
                        cboContacts.Items.Add(String.Concat(contact.ContactName, " (", contact.JobRole, ")"))
                    End If
                Next
                If (cboContacts.Items.Count > 0) Then
                    cboContacts.SelectedIndex = 0
                End If

            End Using
        End If
    End Sub

    Private Sub AddGenericInfo(ByVal myDoc As Document)
        Dim num As Double
        Dim refNo As Object
        Dim variable As Document = myDoc
        If (variable.Bookmarks.Exists("Date")) Then
            refNo = "Date"
            variable.Bookmarks.Item(refNo).Range.Text = Microsoft.VisualBasic.Strings.Format(DateAndTime.Today, "dd MMM yyyy")
        End If
        variable = Nothing
        Dim variable1 As Document = myDoc
        If (variable1.Bookmarks.Exists("Ref")) Then
            refNo = "Ref"
            variable1.Bookmarks.Item(refNo).Range.Text = _mobjCustomer.RefNo
        End If
        variable1 = Nothing
        Dim variable2 As Document = myDoc
        If (variable2.Bookmarks.Exists("Contact")) Then
            refNo = "Contact"
            variable2.Bookmarks.Item(refNo).Range.Text = cboContacts.Text
        End If
        variable2 = Nothing
        Dim variable3 As Document = myDoc
        If (variable3.Bookmarks.Exists("Home")) Then
            refNo = "Home"
            variable3.Bookmarks.Item(refNo).Range.Text = txtCustomerName.Text
        End If
        variable3 = Nothing
        Dim variable4 As Document = myDoc
        If (variable4.Bookmarks.Exists("Home2")) Then
            refNo = "Home2"
            variable4.Bookmarks.Item(refNo).Range.Text = txtCustomerName.Text
        End If
        variable4 = Nothing
        Dim variable5 As Document = myDoc
        If (variable5.Bookmarks.Exists("Address")) Then
            refNo = "Address"
            If txtInvoiceAddress.Text.Trim() = "" Then
                variable5.Bookmarks.Item(refNo).Range.Text = txtAddress.Text
            Else
                variable5.Bookmarks.Item(refNo).Range.Text = txtInvoiceAddress.Text
            End If
        End If
        variable5 = Nothing
        Double.TryParse(cboPaymentAmount_new.Text, num)
        If cboPaymentType_new.Text = "Monthly Support" Then
            num *= 12
        End If
        Dim variable6 As Document = myDoc
        If (variable6.Bookmarks.Exists("Net")) Then
            refNo = "Net"
            variable6.Bookmarks.Item(refNo).Range.Text = Microsoft.VisualBasic.Strings.Format(num, "0.00")
        End If
        variable6 = Nothing
        Dim variable7 As Document = myDoc
        If (variable7.Bookmarks.Exists("Gross")) Then
            refNo = "Gross"
            variable7.Bookmarks.Item(refNo).Range.Text = Microsoft.VisualBasic.Strings.Format(num * 1.2, "0.00")
        End If
        variable7 = Nothing
        Dim variable8 As Document = myDoc
        If (variable8.Bookmarks.Exists("Monthly")) Then
            refNo = "Monthly"
            variable8.Bookmarks.Item(refNo).Range.Text = Microsoft.VisualBasic.Strings.Format(num * 1.2 / 12, "0.00")
        End If
        variable8 = Nothing
    End Sub

    Private Sub Addpaymentdata()
        If (_mobjCustomer.PaymentPeriod <> 0) Then
            If (_mobjCustomer.PaymentMonth < 1) Then
                cboPaymentMonth.SelectedIndex = -1
            Else
                cboPaymentMonth.SelectedIndex = _mobjCustomer.PaymentMonth - 1
            End If
            cboPaymentMonth_new.SelectedIndex = cboPaymentMonth.SelectedIndex

            cboPaymentType.SelectedIndex = _mobjCustomer.PaymentType
        Else
            cboPaymentType.SelectedIndex = enumPaymentType.None
        End If

        cboPaymentType_new.SelectedIndex = cboPaymentType.SelectedIndex

        Dim supportPackage As String = _mobjCustomer.SupportPackage
        If supportPackage = "Bronze" Then
            txtCustomerName.BackColor = Color.White
            txtCustomerName.Font = New System.Drawing.Font(txtCustomerName.Font, FontStyle.Regular)
            cboPaymentBand.SelectedIndex = 0
        ElseIf supportPackage = "Silver" Then
            cboPaymentBand.SelectedIndex = 1
            txtCustomerName.BackColor = Color.Silver
            txtCustomerName.Font = New System.Drawing.Font(txtCustomerName.Font, FontStyle.Bold)
        ElseIf supportPackage = "Gold" Then
            cboPaymentBand.SelectedIndex = 2
            txtCustomerName.BackColor = Color.Gold
            txtCustomerName.Font = New System.Drawing.Font(txtCustomerName.Font, FontStyle.Bold)
        Else
            cboPaymentBand.SelectedIndex = -1
            txtCustomerName.BackColor = Color.White
            txtCustomerName.Font = New System.Drawing.Font(txtCustomerName.Font, FontStyle.Regular)
        End If

        cboPaymentBand_new.SelectedIndex = cboPaymentBand.SelectedIndex

        cboPaymentAmount.Text = Format(_mobjCustomer.PaymentReceived, "0.00")

        cboPaymentAmount_new.SelectedIndex = cboPaymentAmount.Items.IndexOf(cboPaymentAmount.Text)

        If (cboPaymentAmount_new.SelectedIndex = -1) Then
            cboPaymentAmount_new.Text = ""
        End If

        FormatTextBoxColour()
    End Sub

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExport.Click
        Dim refNo As Integer = 0
        Dim excelLoader As ExcelLoader
        Dim wBook As Microsoft.Office.Interop.Excel.Workbook
        Dim wSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim rng As Microsoft.Office.Interop.Excel.Range

        Try

            Try
                excelLoader = New ExcelLoader(False)
            Catch exception As System.Exception
                MsgBox("The program was unable to open Microsoft Excel. The version installed must be Office 2007 or later.", MsgBoxStyle.Exclamation, "Error Opening Excel")
                Exit Sub
            End Try

            'create a new excel workbook
            wBook = excelLoader.Application.Workbooks.Add()

            wSheet = wBook.ActiveSheet


            Dim boldstyle As Microsoft.Office.Interop.Excel.Style = wBook.Styles.Add("BoldStyle")
            boldstyle.Font.Bold = True

            Dim redstyle As Microsoft.Office.Interop.Excel.Style = wBook.Styles.Add("RedStyle")
            redstyle.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red)
            redstyle.Font.Bold = True

            Dim orangestyle As Microsoft.Office.Interop.Excel.Style = wBook.Styles.Add("OrangeStyle")
            orangestyle.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange)
            orangestyle.Font.Bold = True

            Dim greenstyle As Microsoft.Office.Interop.Excel.Style = wBook.Styles.Add("GreenStyle")
            greenstyle.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Green)
            greenstyle.Font.Bold = True

            For i As Integer = 0 To _gcolCustomers.Count - 1

                Dim customer As clsCustomer = _gcolCustomers(i)
                If (refNo = 0) Then

                    wSheet.Cells(1, 1) = "Customer Ref"
                    wSheet.Cells(1, 2) = "Customer Name"
                    wSheet.Cells(1, 3) = "Payment Type"
                    wSheet.Cells(1, 4) = "Support Package"
                    wSheet.Cells(1, 5) = "Payment Value"
                    rng = wSheet.Range(wSheet.Cells(1, 1), wSheet.Cells(1, 5))
                    rng.Style = boldstyle
                End If

                refNo = refNo + 1

                wSheet.Cells(refNo + 1, 1) = customer.RefNo
                wSheet.Cells(refNo + 1, 2) = customer.CustomerName
                rng = wSheet.Range(wSheet.Cells(refNo + 1, 3), wSheet.Cells(refNo + 1, 3))
                rng.Style = boldstyle
                Select Case customer.PaymentType
                    Case enumPaymentType.Annual
                        wSheet.Cells(refNo + 1, 3) = "Annual Support"
                        Exit Select
                    Case enumPaymentType.Monthly
                        wSheet.Cells(refNo + 1, 3) = "Monthly Support"
                        rng.Style = greenstyle
                        Exit Select
                    Case enumPaymentType.Rental
                        wSheet.Cells(refNo + 1, 3) = "Rental"
                        rng.Style = redstyle
                        Exit Select
                End Select
                wSheet.Cells(refNo + 1, 4) = customer.SupportPackage
                wSheet.Cells(refNo + 1, 5) = customer.PaymentReceived
            Next

            wSheet.Columns.AutoFit()

            excelLoader.Application.Visible = True
            excelLoader.Application.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlMaximized

            excelLoader.Dispose()
            excelLoader = Nothing
        Catch ex As System.Exception
            ErrorMessage(ex)
        End Try
    End Sub

    Private Sub btnNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNext.Click
        If (_mcm IsNot Nothing AndAlso _mcm.Position < [Global]._gcolCustomers.Count) Then
            Dim position As CurrencyManager = _mcm
            position.Position = position.Position + 1
        End If
    End Sub

    Private Sub btnPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrevious.Click
        If (_mcm IsNot Nothing AndAlso _mcm.Position > 0) Then
            Dim position As CurrencyManager = _mcm
            position.Position = position.Position - 1
        End If
    End Sub

    Private Sub btnProcess_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnProcess.Click
        SavePaymentdetails()
        SaveNote()
        PrintReminder()
    End Sub

    Private Sub cboMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboMonth.SelectedIndexChanged
        Fillcustomers()
    End Sub

    Private Sub Fillcustomers()
        Dim list As List(Of clsCustomer)
        Dim colCustomer As colCustomers = New colCustomers()
        Dim colCustomer1 As colCustomers = New colCustomers()

        If (_gcolCustomers IsNot Nothing) Then
            _mcm = DirectCast(BindingContext(_gcolCustomers), CurrencyManager)
            If (_mcm IsNot Nothing) Then
                RemoveHandler _mcm.CurrentChanged, New EventHandler(AddressOf Customer_Changed)
            End If
            _gcolCustomers.Dispose()
            _gcolCustomers = Nothing
        End If

        If rbPurchase.Checked Then
            colCustomer.SelectAll_bySQL(String.Concat("SELECT * FROM tblCustomers WHERE tblCustomers.PaymentMonth = ", cboMonth.SelectedIndex + 1, " AND Status = 'Customer' ORDER BY CustomerName"))
        Else
            colCustomer1.SelectAll_bySQL("select * from tblCustomers where DateBecameCustomer < '02/01/2020' and status = 'Customer' and PaymentType = 3 order by CustomerName")
        End If

        If (cboMonth.SelectedIndex <> 0) Then
            list = colCustomer.Except(colCustomer1).ToList()
            list = colCustomer.Where(Function(c As clsCustomer) Not colCustomer1.Any(Function(x As clsCustomer) x.CustomerID = c.CustomerID)).ToList()
        Else
            list = colCustomer.Union(colCustomer1, New CustomerComparer()).ToList()
        End If

        _gcolCustomers = New colCustomers(list.OrderBy(Of String)(Function(x As clsCustomer) x.CustomerName).ToList())
        _mcm = DirectCast(BindingContext([Global]._gcolCustomers), CurrencyManager)

        AddHandler _mcm.CurrentChanged, New EventHandler(AddressOf Customer_Changed)
        _mcm.Refresh()
    End Sub

    Private Sub cboPaymentAmount_New_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboPaymentAmount_new.TextChanged
        If cboPaymentAmount_new.Text = cboPaymentAmount.Text Then
            cboPaymentAmount_new.BackColor = Color.White
        Else
            cboPaymentAmount_new.BackColor = Color.Cyan
        End If
    End Sub

    Private Sub Customer_Changed(ByVal sender As Object, ByVal e As EventArgs)
        _mobjCustomer = [Global]._gcolCustomers(_mcm.Position)

        lblRecords.Text = String.Concat("Record ", (_mcm.Position + 1), " of ", _gcolCustomers.Count)
        AddCustomerBindings()
        Addpaymentdata()
    End Sub

    Private Function DetermineFileName(Optional ByVal strExtension As String = "pdf") As String
        Dim defaultDir As String = GetDefaultDir()
        If defaultDir Is Nothing OrElse defaultDir = "" Then
            Return ""
        End If
        Dim str As String = Path.Combine(defaultDir, String.Concat(_mobjCustomer.CustomerName, " - Price Increase Letter 2017.docx"))
        Return str
    End Function


    Private Sub FillcboPaymentamount(ByVal cbo As ComboBox, ByVal _alPayments As ArrayList)
        cbo.Items.Clear()
        Dim num As Integer = 0
        Dim count As Integer = _alPayments.Count - 1
        num = 0
        Do
            cbo.Items.Add(_alPayments(num))
            num = num + 1
        Loop While num <= count
        cbo.SelectedIndex = -1
    End Sub

    Private Sub FillcbopaymentMonth(ByVal cbo As ComboBox)
        cbo.Items.Clear()
        cbo.Items.Add("January")
        cbo.Items.Add("February")
        cbo.Items.Add("March")
        cbo.Items.Add("April")
        cbo.Items.Add("May")
        cbo.Items.Add("June")
        cbo.Items.Add("July")
        cbo.Items.Add("August")
        cbo.Items.Add("September")
        cbo.Items.Add("October")
        cbo.Items.Add("November")
        cbo.Items.Add("December")
    End Sub

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        [Global]._gconnectionProvider = New clsConnectionProvider()
        If (Not ValidateUser()) Then
            Interaction.MsgBox("Your user credentials could not be validated", MsgBoxStyle.OkOnly, Nothing)
            Close()
        End If
        SetArrayLists()
        initialisePayments()
        cboMonth.SelectedIndex = 0
    End Sub

    Public Function GetDefaultDir() As String
        Dim str As String = Nothing
        Try
            Dim fullName As String = [Global].AppString("Main.DocumentStorage")
            Dim directoryInfo As System.IO.DirectoryInfo = New System.IO.DirectoryInfo(fullName)
            If (directoryInfo.Exists) Then
                If _mobjCustomer.DefaultDir = "" Then
                    fullName = Path.Combine(fullName, _mobjCustomer.DefaultDir)
                Else
                    fullName = directoryInfo.FullName
                    If (Not fullName.EndsWith("\")) Then
                        fullName = String.Concat(fullName, "\")
                    End If
                    If (_mobjCustomer.CustomerID <> 0) Then
                        fullName = Path.Combine(fullName, String.Concat(txtCustomerName.Text, " ", _mobjCustomer.CustomerID))
                    End If
                End If
                If (Not Directory.Exists(fullName)) Then
                    Directory.CreateDirectory(fullName)
                End If
                str = Microsoft.VisualBasic.Strings.UCase(fullName)
            Else
                Interaction.MsgBox(String.Format("Cannot access {0}. Check that the file exists and you have permission to access it.", fullName), MsgBoxStyle.Information, "File I/O Error")
                str = ""
            End If
        Catch securityException As System.Security.SecurityException
            MsgBox("Security error. Please contact your administrator for details.", Nothing, MsgBoxStyle.OkOnly)
        Catch exception As System.Exception
            ErrorMessage(exception, "", False)
            str = ""
        End Try
        Return str
    End Function

    Private Sub initialisePayments()
        cboPaymentBand.Items.Clear()
        cboPaymentBand.Items.Add("Bronze")
        cboPaymentBand.Items.Add("Silver")
        cboPaymentBand.Items.Add("Gold")
        cboPaymentBand_new.Items.Clear()
        cboPaymentBand_new.Items.Add("Bronze")
        cboPaymentBand_new.Items.Add("Silver")
        cboPaymentBand_new.Items.Add("Gold")
        cboPaymentType.Items.Clear()
        cboPaymentType_new.Items.Clear()
        FillcbopaymentMonth(cboPaymentMonth)
        FillcbopaymentMonth(cboPaymentMonth_new)

        [Global].FillLookup(cboPaymentType, 11, False, False)
        [Global].FillLookup(cboPaymentType_new, 11, False, False)
    End Sub

    Private Sub NewPaymentBandChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboPaymentBand_new.SelectedIndexChanged, cboPaymentType_new.SelectedIndexChanged

        Select Case cboPaymentBand_new.Text
            Case "Bronze"
                If cboPaymentType.SelectedIndex = enumPaymentType.Annual Then
                    FillcboPaymentamount(cboPaymentAmount_new, _malBronzeAnnual_New)
                ElseIf cboPaymentType.SelectedIndex = enumPaymentType.Monthly Then
                    FillcboPaymentamount(cboPaymentAmount_new, _malBronzeMonthly_New)
                Else
                    FillcboPaymentamount(cboPaymentAmount_new, _malBronzeRental_New)
                End If

            Case "Silver"
                If cboPaymentType.SelectedIndex = enumPaymentType.Annual Then
                    FillcboPaymentamount(cboPaymentAmount_new, _malSilverAnnual_New)
                ElseIf cboPaymentType.SelectedIndex = enumPaymentType.Monthly Then
                    FillcboPaymentamount(cboPaymentAmount_new, _malSilverMonthly_New)
                Else
                    FillcboPaymentamount(cboPaymentAmount_new, _malSilverRental_New)
                End If
            Case "Gold"
                If cboPaymentType.SelectedIndex = enumPaymentType.Annual Then
                    FillcboPaymentamount(cboPaymentAmount_new, _malGoldAnnual_New)
                ElseIf cboPaymentType.SelectedIndex = enumPaymentType.Monthly Then
                    FillcboPaymentamount(cboPaymentAmount_new, _malGoldMonthly_New)
                Else
                    FillcboPaymentamount(cboPaymentAmount_new, _malGoldRental_New)
                End If

        End Select
    End Sub

    Private Sub PaymentBandChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboPaymentBand.SelectedIndexChanged, cboPaymentType.SelectedIndexChanged

        Select Case cboPaymentBand.SelectedItem
            Case "Bronze"
                If cboPaymentType.SelectedIndex = enumPaymentType.Annual Then
                    FillcboPaymentamount(cboPaymentAmount, _malBronzeAnnual)
                ElseIf cboPaymentType.SelectedIndex = enumPaymentType.Monthly Then
                    FillcboPaymentamount(cboPaymentAmount, _malBronzeMonthly)
                Else
                    FillcboPaymentamount(cboPaymentAmount, _malBronzeRental)
                End If

            Case "Silver"
                If cboPaymentType.SelectedIndex = enumPaymentType.Annual Then
                    FillcboPaymentamount(cboPaymentAmount, _malSilverAnnual)
                ElseIf cboPaymentType.SelectedIndex = enumPaymentType.Monthly Then
                    FillcboPaymentamount(cboPaymentAmount, _malSilverMonthly)
                Else
                    FillcboPaymentamount(cboPaymentAmount, _malSilverRental)
                End If
            Case "Gold"
                If cboPaymentType.SelectedIndex = enumPaymentType.Annual Then
                    FillcboPaymentamount(cboPaymentAmount, _malGoldAnnual)
                ElseIf cboPaymentType.SelectedIndex = enumPaymentType.Monthly Then
                    FillcboPaymentamount(cboPaymentAmount, _malGoldMonthly)
                Else
                    FillcboPaymentamount(cboPaymentAmount, _malGoldRental)
                End If

        End Select

        FormatTextBoxColour()
    End Sub

    Private Sub FormatTextBoxColour()
        If cboPaymentBand_new.Text = cboPaymentBand.Text Then
            cboPaymentBand_new.BackColor = Color.White
        Else
            cboPaymentBand_new.BackColor = Color.Cyan
        End If
        If cboPaymentMonth_new.Text = cboPaymentMonth.Text Then
            cboPaymentMonth_new.BackColor = Color.White
        Else
            cboPaymentMonth_new.BackColor = Color.Cyan
        End If
        If cboPaymentType_new.Text = cboPaymentType.Text Then
            cboPaymentType_new.BackColor = Color.White
        Else
            cboPaymentType_new.BackColor = Color.Cyan
        End If
    End Sub

    Public Sub PrintReminder()
        Dim template As String
        Dim doc As Microsoft.Office.Interop.Word.Document

        Dim wordLoader As New WordLoader(False)
        Try
            Try
                If Not (wordLoader.WordIsInstalled) Then

                    Interaction.MsgBox("Microsoft Word is not installed on this machine. You will be unable to make use of this feature", MsgBoxStyle.Information, "CMS")
                    wordLoader.Dispose()
                    wordLoader = Nothing
                    Exit Sub
                End If

                If cboPaymentType_new.Text = "Rental" Then
                    template = [Global].AppString("Main.PriceIncreaseRentalsDoc")
                Else
                    template = [Global].AppString("Main.PriceIncreaseDoc")
                End If

                Dim fileName As String = DetermineFileName(".docx")

                If String.IsNullOrWhiteSpace(fileName) Then Exit Sub

                If IO.File.Exists(fileName) Then

                    wordLoader.Application.Documents.Open(fileName)



                Else
                    wordLoader.Application.Documents.Open(template)

                    doc = wordLoader.Application.ActiveDocument

                    AddGenericInfo(doc)

                    doc.Protect(Word.WdProtectionType.wdAllowOnlyFormFields, True)
                    doc.SaveAs(FileName:=fileName)



                End If

                wordLoader.Application.Visible = True
                wordLoader.Application.WindowState = Word.WdWindowState.wdWindowStateMaximize

            Catch exception As System.Exception
                ErrorMessage(exception, "", False)
            End Try
        Finally
            wordLoader.Dispose()
            wordLoader = Nothing
        End Try
    End Sub

    Private Sub SaveNote()
        Try
            Using _clsNote As clsNote = New clsNote()
                _clsNote.SubjectType = 61
                _clsNote.CustomerID = _mobjCustomer.CustomerID
                _clsNote.CustomerName = _mobjCustomer.CustomerName
                _clsNote.DateLogged = Now
                _clsNote.RecordedBy = [Global]._gloggedOnUser.Fullname
                _clsNote.Notes = String.Format("Price Increase Notification Letter Issues. {0} Charge to be increase to £{1}", cboPaymentType_new.Text, cboPaymentAmount_new.Text)
                _clsNote.SaveRecord()
            End Using
        Catch exception As System.Exception
            ErrorMessage(exception, "", False)
        End Try
    End Sub

    Private Sub SavePaymentdetails()

        Double.TryParse(cboPaymentAmount_new.Text, _mobjCustomer.PaymentReceived)

        Select Case cboPaymentType_new.SelectedItem
            Case "Annual Support"
                _mobjCustomer.PaymentType = enumPaymentType.Annual
                _mobjCustomer.PaymentPeriod = 12
            Case "Monthly Support"
                _mobjCustomer.PaymentType = enumPaymentType.Monthly
                _mobjCustomer.PaymentPeriod = 1
            Case "Rental"
                _mobjCustomer.PaymentType = enumPaymentType.Rental
                _mobjCustomer.PaymentPeriod = 1
            Case Else
                _mobjCustomer.PaymentType = enumPaymentType.None
                _mobjCustomer.PaymentReceived = Decimal.Zero
        End Select


        _mobjCustomer.UpdatePaymentdetails()

    End Sub

    Private Sub SetArrayLists()
        _malBronzeAnnual.Add("250.00")
        _malBronzeAnnual.Add("330.00")
        _malBronzeAnnual.Add("440.00")
        _malBronzeAnnual.Add("550.00")
        _malBronzeAnnual.Add("Other")
        _malBronzeMonthly.Add("20.84")
        _malBronzeMonthly.Add("27.50")
        _malBronzeMonthly.Add("36.67")
        _malBronzeMonthly.Add("45.84")
        _malBronzeMonthly.Add("Other")
        _malBronzeRental.Add("73.00")
        _malBronzeRental.Add("105.00")
        _malBronzeRental.Add("140.00")
        _malBronzeRental.Add("175.00")
        _malBronzeRental.Add("Other")
        _malBronzeAnnual_New.Add("275.00")
        _malBronzeAnnual_New.Add("360.00")
        _malBronzeAnnual_New.Add("490.00")
        _malBronzeAnnual_New.Add("605.00")
        _malBronzeMonthly_New.Add("22.92")
        _malBronzeMonthly_New.Add("30.00")
        _malBronzeMonthly_New.Add("40.83")
        _malBronzeMonthly_New.Add("50.42")
        _malBronzeRental_New.Add("80.00")
        _malBronzeRental_New.Add("115.00")
        _malBronzeRental_New.Add("155.00")
        _malBronzeRental_New.Add("190.00")
        _malSilverAnnual.Add("850.00")
        _malSilverAnnual.Add("930.00")
        _malSilverAnnual.Add("1040.00")
        _malSilverAnnual.Add("1150.00")
        _malSilverAnnual.Add("Other")
        _malSilverMonthly.Add("70.84")
        _malSilverMonthly.Add("77.50")
        _malSilverMonthly.Add("86.67")
        _malSilverMonthly.Add("95.84")
        _malSilverMonthly.Add("Other")
        _malSilverRental.Add("123.00")
        _malSilverRental.Add("155.00")
        _malSilverRental.Add("190.00")
        _malSilverRental.Add("225.00")
        _malSilverRental.Add("Other")
        _malSilverAnnual_New.Add("875.00")
        _malSilverAnnual_New.Add("960.00")
        _malSilverAnnual_New.Add("1090.00")
        _malSilverAnnual_New.Add("1205.00")
        _malSilverMonthly_New.Add("72.92")
        _malSilverMonthly_New.Add("80.00")
        _malSilverMonthly_New.Add("90.83")
        _malSilverMonthly_New.Add("100.42")
        _malSilverRental_New.Add("130.00")
        _malSilverRental_New.Add("165.00")
        _malSilverRental_New.Add("205.00")
        _malSilverRental_New.Add("240.00")
        _malGoldAnnual.Add("1650.00")
        _malGoldAnnual.Add("1730.00")
        _malGoldAnnual.Add("1840.00")
        _malGoldAnnual.Add("1950.00")
        _malGoldAnnual.Add("Other")
        _malGoldMonthly.Add("137.50")
        _malGoldMonthly.Add("144.17")
        _malGoldMonthly.Add("153.34")
        _malGoldMonthly.Add("162.50")
        _malGoldMonthly.Add("Other")
        _malGoldRental.Add("190.00")
        _malGoldRental.Add("225.00")
        _malGoldRental.Add("260.00")
        _malGoldRental.Add("295.00")
        _malGoldRental.Add("Other")
        _malGoldAnnual_New.Add("1670.00")
        _malGoldAnnual_New.Add("1760.00")
        _malGoldAnnual_New.Add("1890.00")
        _malGoldAnnual_New.Add("2005.00")
        _malGoldMonthly_New.Add("139.17")
        _malGoldMonthly_New.Add("146.67")
        _malGoldMonthly_New.Add("157.50")
        _malGoldMonthly_New.Add("167.08")
        _malGoldRental_New.Add("197.00")
        _malGoldRental_New.Add("232.00")
        _malGoldRental_New.Add("272.00")
        _malGoldRental_New.Add("307.00")
    End Sub

    Public Sub Stream2File(ByVal InputStream As Stream, ByVal OutputPath As String)
        Using fileStream As System.IO.FileStream = New System.IO.FileStream(OutputPath, FileMode.Create, FileAccess.Write)
            Dim numArray(256) As Byte
            Dim num As Integer = InputStream.Read(numArray, 0, 256)
            While num > 0
                fileStream.Write(numArray, 0, num)
                num = InputStream.Read(numArray, 0, 256)
            End While
            InputStream.Flush()
            InputStream.Close()
            InputStream.Dispose()
            InputStream = Nothing
        End Using
    End Sub

    Private Function ValidateUser() As Boolean
        Dim _clsUser As clsUser
        Dim flag As Boolean = False
        Try
            Try
                _clsUser = New clsUser(True)
                Dim windowsPrincipal As System.Security.Principal.WindowsPrincipal = New System.Security.Principal.WindowsPrincipal(WindowsIdentity.GetCurrent())
                _clsUser.SelectOne(windowsPrincipal.Identity.Name)
                If (_clsUser.UserID <> 0) Then
                    _gloggedOnUser = _clsUser
                    flag = True
                End If
            Catch exception As System.Exception
                ErrorMessage(exception)
                flag = True
            End Try
        Finally
            _clsUser = Nothing
        End Try
        Return flag
    End Function

    Private Sub rbPurchase_CheckedChanged(sender As Object, e As EventArgs) Handles rbPurchase.CheckedChanged
        If Me.Visible AndAlso rbPurchase.Checked Then Fillcustomers()
    End Sub

    Private Sub rbRental_CheckedChanged(sender As Object, e As EventArgs) Handles rbRental.CheckedChanged
        If Me.Visible AndAlso rbRental.Checked Then Fillcustomers()
    End Sub
End Class
