﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.IO
Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Text
Imports Customers_Classes
Imports Customers_Classes.Connection
Imports Microsoft.VisualBasic.CompilerServices

Public Module [Global]
    Public _gCustomersVersion As Object

    Public _gconnectionProvider As clsConnectionProvider

    Public _gloggedOnUser As clsUser

    Public _gcolCustomers As colCustomers

    Public _gRemoteDatabaseServers As List(Of String)

    Sub New()
        [Global]._gCustomersVersion = Application.ProductVersion.ToString()
        [Global]._gRemoteDatabaseServers = New List(Of String)()
    End Sub

    Public Function AppString(ByVal strKey As String) As String
        Dim str As String
        Dim appSettingsReader As System.Configuration.AppSettingsReader = New System.Configuration.AppSettingsReader()
        Try
            str = appSettingsReader.GetValue(strKey, "".[GetType]()).ToString()
        Catch exception As System.Exception
            ProjectData.SetProjectError(exception)
            str = ""
            ProjectData.ClearProjectError()
        End Try
        Return str
    End Function

    Public Function ConvertSortedListToString(ByVal sl As SortedList) As String
        Dim dictionaryEntry As System.Collections.DictionaryEntry = New System.Collections.DictionaryEntry()
        Dim dictionaryEntry1 As System.Collections.DictionaryEntry
        Dim stringBuilder As System.Text.StringBuilder = New System.Text.StringBuilder()
        If (sl Is Nothing) Then
            Return ""
        End If
        Dim enumerator As IDictionaryEnumerator = sl.GetEnumerator()
        While enumerator.MoveNext()
            Dim current As Object = enumerator.Current
            If (current IsNot Nothing) Then
                dictionaryEntry1 = DirectCast(current, System.Collections.DictionaryEntry)
            Else
                dictionaryEntry1 = dictionaryEntry
            End If
            Dim dictionaryEntry2 As System.Collections.DictionaryEntry = dictionaryEntry1
            stringBuilder.Append(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(dictionaryEntry2.Value, Strings.Space(3)))
        End While
        Return stringBuilder.ToString()
    End Function

    Public Sub ErrorMessage(ByVal ex As Exception, Optional ByVal strExtra As String = "", Optional ByVal blnUnhandled As Boolean = False)
        If (TypeOf ex Is SqlException AndAlso ex.Message.StartsWith("Timeout expired")) Then
            Dim newLine() As String = {"The database has timed out whilst trying to retreive data.", Environment.NewLine, "This could be due to an excessive amount of activity on your machine.", Environment.NewLine, Environment.NewLine, "Some data will not have been retreived in this transaction."}
            Interaction.MsgBox(String.Concat(newLine), MsgBoxStyle.Exclamation, Nothing)
            Return
        End If
        Dim frame As StackFrame = (New StackTrace(1, True)).GetFrame(0)
        Dim stringBuilder As System.Text.StringBuilder = New System.Text.StringBuilder()
        If (frame Is Nothing) Then
            stringBuilder.Append(ex.Message)
        Else
            Dim method As MethodBase = frame.GetMethod()
            If (Microsoft.VisualBasic.CompilerServices.Operators.CompareString(strExtra, "", False) <> 0) Then
                stringBuilder.Append(String.Concat(strExtra, Environment.NewLine))
            End If
            stringBuilder.Append("Error in File ")
            stringBuilder.Append(frame.GetFileName())
            stringBuilder.Append(", line ")
            stringBuilder.Append(frame.GetFileLineNumber())
            stringBuilder.Append(Environment.NewLine)
            stringBuilder.Append("in method : ")
            stringBuilder.Append(method.ReflectedType.Name)
            stringBuilder.Append(".")
            stringBuilder.Append(method.Name)
            stringBuilder.Append(String.Concat(Environment.NewLine, Environment.NewLine, ex.Message))
        End If
        If (ex.InnerException IsNot Nothing) Then
            stringBuilder.Append(String.Concat("", ex.InnerException.Message))
            If (ex.InnerException.InnerException IsNot Nothing) Then
                stringBuilder.Append(String.Concat("", ex.InnerException.InnerException.Message))
            End If
        End If
        Interaction.MsgBox(stringBuilder.ToString(), MsgBoxStyle.OkOnly, Nothing)
    End Sub

    Public Sub ExecuteSQL(ByVal strSQL As String)
        Dim sqlCommand As System.Data.SqlClient.SqlCommand = Nothing
        Try
            Try
                Using sqlConnection As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection([Global]._gconnectionProvider.ConnectionString)
                    sqlConnection.Open()
                    sqlCommand = New System.Data.SqlClient.SqlCommand(strSQL.Trim(), sqlConnection) With
                    {
                        .CommandType = CommandType.Text
                    }
                    sqlCommand.ExecuteNonQuery()
                    sqlConnection.Close()
                End Using
            Catch exception As System.Exception
                ProjectData.SetProjectError(exception)
                [Global].ErrorMessage(exception, "", False)
                ProjectData.ClearProjectError()
            End Try
        Finally
            sqlCommand.Dispose()
        End Try
    End Sub

    Public Function ExecuteSQL_WithReturnValue(ByVal strSQL As String) As Object
        Dim sqlCommand As System.Data.SqlClient.SqlCommand = Nothing
        Dim objectValue As Object = Nothing
        Try
            Try
                Using sqlConnection As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection([Global]._gconnectionProvider.ConnectionString)
                    sqlConnection.Open()
                    sqlCommand = New System.Data.SqlClient.SqlCommand(strSQL, sqlConnection) With
                    {
                        .CommandType = CommandType.Text
                    }
                    objectValue = RuntimeHelpers.GetObjectValue(sqlCommand.ExecuteScalar())
                    sqlConnection.Close()
                End Using
            Catch exception As System.Exception
                ProjectData.SetProjectError(exception)
                [Global].ErrorMessage(exception, "", False)
                ProjectData.ClearProjectError()
            End Try
        Finally
            sqlCommand.Dispose()
        End Try
        Return objectValue
    End Function

    Public Sub FillLookup(ByVal cbo As ComboBox, ByVal lngLookupID As Integer, ByVal blnAll As Boolean, Optional ByVal UseDatabinding As Boolean = False)
        Try
            Dim colLookup As colLookups = New colLookups()
            colLookup.SelectAll(lngLookupID)
            cbo.DataSource = Nothing
            If (blnAll) Then
                colLookup.Insert(0, New clsLookup(0, "(All)"))
            End If
            If (Not UseDatabinding) Then
                cbo.Items.Clear()
                Dim count As Integer = colLookup.Count - 1
                Dim num As Integer = 0
                Do
                    cbo.Items.Add(colLookup(num).Description)
                    num = num + 1
                Loop While num <= count
                If (cbo.Items.Count > 0) Then
                    cbo.SelectedIndex = 0
                End If
            Else
                cbo.DataSource = colLookup
                cbo.DisplayMember = "Description"
                cbo.ValueMember = "LookupID"
            End If
            colLookup = Nothing
        Catch exception As System.Exception
            ProjectData.SetProjectError(exception)
            [Global].ErrorMessage(exception, "Subroutines.FillLookup", False)
            ProjectData.ClearProjectError()
        End Try
    End Sub

    Public Function GetEmbeddedImage(ByVal strImageName As String) As Image
        Dim bitmap As Image = Nothing
        Try
            Dim executingAssembly As Assembly = Assembly.GetExecutingAssembly()
            Dim [namespace] As String = GetType([Global]).[Namespace]
            If (Microsoft.VisualBasic.CompilerServices.Operators.CompareString([namespace], "", False) <> 0) Then
                strImageName = String.Concat([namespace], ".", strImageName)
            End If
            Dim manifestResourceStream As Stream = executingAssembly.GetManifestResourceStream(strImageName)
            If (manifestResourceStream IsNot Nothing) Then
                bitmap = New System.Drawing.Bitmap(manifestResourceStream)
            End If
        Catch exception As System.Exception
            ProjectData.SetProjectError(exception)
            ProjectData.ClearProjectError()
        End Try
        Return bitmap
    End Function

    Public Sub HourglassOff()
        Cursor.Current = Cursors.[Default]
    End Sub

    Public Sub HourglassOn()
        Cursor.Current = Cursors.WaitCursor
        Cursor.Show()
    End Sub

    Public Function IsKeyNumeric(ByVal e As KeyPressEventArgs) As Integer
        Dim num As Integer = 0
        If (Not Char.IsDigit(e.KeyChar) AndAlso Not Char.IsControl(e.KeyChar) AndAlso Microsoft.VisualBasic.CompilerServices.Operators.CompareString(Conversions.ToString(e.KeyChar), "-", False) <> 0 AndAlso Microsoft.VisualBasic.CompilerServices.Operators.CompareString(Conversions.ToString(e.KeyChar), ".", False) <> 0) Then
            e.Handled = True
        End If
        Return num
    End Function

    Public Function OpenFile(ByVal strFileName As String) As Integer
        'Dim flag As Boolean
        'Dim str As String
        'Dim num As Integer = 0
        'Try
        '	num = -1
        '	If (File.Exists(strFileName)) Then
        '		Boolean.TryParse([Global].AppString("Main.BuiltInImagePreview"), flag)
        '		Dim desktopWindow As Integer = CInt(clsNativeMethods.GetDesktopWindow())
        '		num = CInt(clsNativeMethods.ShellExecute(DirectCast(desktopWindow, IntPtr), Nothing, Path.GetFileName(strFileName), "", Path.GetDirectoryName(strFileName), 1))
        '		If (num <= 32) Then
        '			Select Case num
        '				Case 2
        '					str = "File not found"
        '					Exit Select
        '				Case 3
        '					str = "Path not found"
        '					Exit Select
        '				Case 4
        '				Case 6
        '				Case 7
        '				Case 9
        '				Case 10
        '				Case 12
        '				Case 13
        '				Case 14
        '				Case 15
        '				Case 16
        '				Case 17
        '				Case 18
        '				Case 19
        '				Case 20
        '				Case 21
        '				Case 22
        '				Case 23
        '				Case 24
        '				Case 25
        '				Label0:
        '					str = "Unknown error"
        '					Exit Select
        '				Case 5
        '					str = "Access denied"
        '					Exit Select
        '				Case 8
        '					str = "Out of memory"
        '					Exit Select
        '				Case 11
        '					str = "Invalid EXE file or error in EXE image"
        '					Exit Select
        '				Case 26
        '					str = "A sharing violation occurred"
        '					Exit Select
        '				Case 27
        '					str = "Incomplete or invalid file association"
        '					Exit Select
        '				Case 28
        '					str = "DDE Time out"
        '					Exit Select
        '				Case 29
        '					str = "DDE transaction failed"
        '					Exit Select
        '				Case 30
        '					str = "DDE busy"
        '					Exit Select
        '				Case 31
        '					str = "No association for file extension"
        '					Exit Select
        '				Case 32
        '					str = "DLL not found"
        '					Exit Select
        '				Case Else
        '					GoTo Label0
        '			End Select
        '			Interaction.MsgBox(str, MsgBoxStyle.Information, Nothing)
        '			num = 0
        '		End If
        '	Else
        '		Interaction.MsgBox("This file does not exist", MsgBoxStyle.Exclamation, Nothing)
        '		num = 0
        '	End If
        'Catch exception As System.Exception
        '	ProjectData.SetProjectError(exception)
        '	[Global].ErrorMessage(exception, "", False)
        '	ProjectData.ClearProjectError()
        'End Try
        'Return num
    End Function

    Public Function PrepareSQLDate(ByVal strValue As DateTime, Optional ByVal includeTime As Boolean = False) As String
        If (DateTime.Compare(strValue, DateTime.MinValue) = 0) Then
            Return "NULL"
        End If
        If (Not includeTime) Then
            Return String.Concat("'", strValue.ToString("yyyy-MM-dd"), "'")
        End If
        Return String.Concat("'", strValue.ToString("yyyy-MM-dd HH:mm:ss.fff"), "'")
    End Function

    Public Function PrepareSQLStr(ByVal strValue As String) As String
        If (strValue Is Nothing OrElse Microsoft.VisualBasic.CompilerServices.Operators.CompareString(strValue.Trim(), "", False) = 0) Then
            Return "''"
        End If
        Dim str As String = strValue
        If (Strings.InStr(strValue, "'", CompareMethod.Binary) <> 0) Then
            str = Strings.Replace(strValue, "'", "''", 1, -1, CompareMethod.Binary)
        End If
        Return str.Trim()
    End Function

End Module