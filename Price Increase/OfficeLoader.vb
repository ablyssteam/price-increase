﻿Imports System.Runtime.InteropServices
Imports Microsoft.Office.Interop

Public Class ExcelLoader

    'Implements IDisposable 'ToDo: Add Implements Clauses for implementation methods of these interface(s)
    Private ExcelApp As Object

    Private isNewApp As Boolean = False

    Private disposed As Boolean = False

    Private _excelIsInstalled As Boolean

    Public ReadOnly Property ExcelIsInstalled As Boolean
        Get
            Return _excelIsInstalled
        End Get
    End Property

    Private _excelVersion As Single

    Public ReadOnly Property ExcelVersion As Single
        Get
            Return _excelVersion
        End Get
    End Property

    Public Sub New(Optional ByVal OnlyCheckIfInstalledAndVersion As Boolean = False)

        CheckIfMsExcelInstalledAndVersion()
        If _excelIsInstalled = False OrElse OnlyCheckIfInstalledAndVersion = True Then
            Exit Sub
        End If

        'Check if Excel is registered in the ROT.
        Try
            ExcelApp = Marshal.GetActiveObject("Excel.Application")
        Catch
        End Try
        ' Load Excel if it's not.
        If ExcelApp Is Nothing Then
            Try
                ExcelApp = CreateObject("Excel.Application")
                isNewApp = True
            Catch
            End Try
        End If
    End Sub 'New

    ''' <summary>
    ''' Checks whether MS Excel is installed and its version (checks registry and then if exe really exists).
    ''' </summary>
    Private Sub CheckIfMsExcelInstalledAndVersion()
        'This Function searches the registry for Excel.Application key in ClassessRoot
        'Then it still checks if the exe exists

        Dim excelHandler As String
        Dim regKey As Microsoft.Win32.RegistryKey

        Try
            regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("Excel.Application").OpenSubKey("CLSID")
        Catch ex As Exception
            'If any subkey doesn't exist, will thrown an exception.
            _excelIsInstalled = False
            Exit Sub
        End Try

        If regKey IsNot Nothing Then _
            excelHandler = regKey.GetValue("") 'Get CLSID

        If excelHandler.Length > 0 Then
            'Now find CLSID key:
            '64 Bits HKEY_CLASSES_ROOT\Wow6432Node\CLSID\ --> {000209FF-0000-0000-C000-000000000046}\LocalServer32
            '32 Bits HKEY_CLASSES_ROOT\CLSID\ --> {000209FF-0000-0000-C000-000000000046}\LocalServer32
            regKey = Nothing

            Try 'Try 64 bits first
                regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("Wow6432Node\CLSID").OpenSubKey(excelHandler & "\LocalServer32")
            Catch ex As Exception
                'Not found 64 bits Excel
            End Try

            If regKey Is Nothing Then
                Try 'Try 32 bits first
                    regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("CLSID").OpenSubKey(excelHandler & "\LocalServer32")
                Catch ex As Exception
                    'Not found 32 bits Excel either
                    _excelIsInstalled = False
                    Exit Sub
                End Try
            End If

            excelHandler = regKey.GetValue("") 'Gets the path of the exe.

            'On here it's likely that Excel is Installed as the registry says.
            'The block below now still checks whether the exe exists.
        End If

        If excelHandler.Length > 0 Then
            'Sanitize path
            excelHandler = excelHandler.Replace("""", "")
            excelHandler = excelHandler.Replace("@", "")
            excelHandler = excelHandler.Replace("%1", "")

            If excelHandler.ToLower.Contains(".exe") Then
                excelHandler = Trim(Strings.Left(excelHandler, (InStr(excelHandler.ToLower, ".exe") + 3)))
            ElseIf excelHandler.ToLower.Contains(".dll") Then
                excelHandler = Trim(Strings.Left(excelHandler, (InStr(excelHandler.ToLower, ".dll") + 3)))
            End If

            Try
                If IO.File.Exists(excelHandler) Then

                    Try
                        Dim fileDescr As FileVersionInfo
                        fileDescr = FileVersionInfo.GetVersionInfo(excelHandler)

                        If _
                        InStr(fileDescr.ProductVersion.ToString, ".") >= 2 AndAlso IsNumeric(Strings.Left(fileDescr.ProductVersion.ToString, (InStr(fileDescr.ProductVersion.ToString, ".") - 1))) Then
                            _excelVersion = Strings.Left(fileDescr.ProductVersion.ToString, (InStr(fileDescr.ProductVersion.ToString, ".") - 1))

                        ElseIf IsNumeric(fileDescr.ProductVersion.ToString) Then
                            _excelVersion = fileDescr.ProductVersion.ToString

                        ElseIf _
                        InStr(fileDescr.FileVersion.ToString, ".") >= 2 AndAlso IsNumeric(Strings.Left(fileDescr.FileVersion.ToString, (InStr(fileDescr.FileVersion.ToString, ".") - 1))) Then
                            _excelVersion = Strings.Left(fileDescr.FileVersion.ToString, (InStr(fileDescr.FileVersion.ToString, ".") - 1))

                        ElseIf IsNumeric(fileDescr.FileVersion.ToString) Then
                            _excelVersion = fileDescr.FileVersion.ToString

                        End If
                    Catch ex As Exception
                        MsgBox("Can't determine MS Excel version." & vbNewLine &
                               "" & vbNewLine &
                               "Reason:" & vbNewLine &
                               ex.Message, MsgBoxStyle.Critical, "Error")
                    End Try

                    _excelIsInstalled = True
                    Exit Sub
                Else
                    _excelIsInstalled = False
                    Exit Sub
                End If
            Catch ex As Exception
                MsgBox("Can't determine if MS Excel is installed." & vbNewLine &
                       "" & vbNewLine &
                       "Reason:" & vbNewLine &
                       ex.Message, MsgBoxStyle.Critical, "Error")
                _excelIsInstalled = False
                Exit Sub
            End Try
        Else
            _excelIsInstalled = False
            Exit Sub
        End If

    End Sub

    Public Overloads Sub Dispose()
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub 'Dispose

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
            End If
            ' Dispose managed resources.

            ' Dispose unmanaged resources.
            If Not (ExcelApp Is Nothing) Then
                Try
                    If isNewApp And ExcelApp.Workbooks.Count = 0 Then
                        Dim arg1 As Object = Excel.XlSaveAction.xlDoNotSaveChanges
                        Dim arg2 As Object = Nothing
                        Dim arg3 As Object = Nothing
                        ExcelApp.Quit()

                        ' Wait until Word shuts down.
                    End If
                Catch
                End Try
                ExcelApp = Nothing
            End If
        End If
        disposed = True
    End Sub 'Dispose

    Overloads Sub Finalize()
        Dispose(False)
        MyBase.Finalize()
    End Sub 'Finalize

    Public ReadOnly Property Application() As Object
        Get
            Return ExcelApp
        End Get
    End Property

End Class 'WordLoader

Public Class WordLoader

    'Implements IDisposable 'ToDo: Add Implements Clauses for implementation methods of these interface(s)
    Private wordApp As Object

    Private isNewApp As Boolean = False

    Private disposed As Boolean = False

    Private _wordIsInstalled As Boolean

    Public ReadOnly Property WordIsInstalled As Boolean
        Get
            Return _wordIsInstalled
        End Get
    End Property

    Private _wordVersion As Single

    Public ReadOnly Property WordVersion As Single
        Get
            Return _wordVersion
        End Get
    End Property

    Public Sub New(Optional ByVal OnlyCheckIfInstalledAndVersion As Boolean = False)

        CheckIfMsWordInstalledAndVersion()
        If _wordIsInstalled = False OrElse OnlyCheckIfInstalledAndVersion = True Then
            Exit Sub
        End If

        'Check if Word is registered in the ROT.
        'Try
        '    wordApp = Marshal.GetActiveObject("Word.Application")
        'Catch
        'End Try
        'Load Word if it's not.
        If wordApp Is Nothing Then
            Try
                wordApp = CreateObject("Word.Application")
                wordApp.DisplayAlerts = Word.WdAlertLevel.wdAlertsNone
                isNewApp = True
            Catch
            End Try
        End If

    End Sub 'New

    ''' <summary>
    ''' Checks whether MS Word is installed and its version (checks registry and then if exe really exists).
    ''' </summary>
    Private Sub CheckIfMsWordInstalledAndVersion()
        'This Function searches the registry for Word.Application key in ClassessRoot
        'Then it still checks if the exe exists

        Dim wordHandler As String
        Dim regKey As Microsoft.Win32.RegistryKey

        Try
            regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("Word.Application").OpenSubKey("CLSID")
        Catch
            'If any subkey doesn't exist, will thrown an exception.
            _wordIsInstalled = False
            Exit Sub
        End Try

        If regKey IsNot Nothing Then _
            wordHandler = regKey.GetValue("") 'Get CLSID

        If Len(wordHandler) > 0 Then
            'Now find CLSID key:
            '64 Bits HKEY_CLASSES_ROOT\Wow6432Node\CLSID\ --> {000209FF-0000-0000-C000-000000000046}\LocalServer32
            '32 Bits HKEY_CLASSES_ROOT\CLSID\ --> {000209FF-0000-0000-C000-000000000046}\LocalServer32
            regKey = Nothing

            Try 'Try 64 bits first
                regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("Wow6432Node\CLSID").OpenSubKey(wordHandler & "\LocalServer32")
            Catch
                'Not found 64 bits Word
            End Try

            If regKey Is Nothing Then
                Try 'Try 32 bits first
                    regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("CLSID").OpenSubKey(wordHandler & "\LocalServer32")
                Catch
                    'Not found 32 bits Word either
                    _wordIsInstalled = False
                    Exit Sub
                End Try
            End If

            wordHandler = regKey.GetValue("") 'Gets the path of the exe.

            'On here it's likely that Word is Installed as the registry says.
            'The block below now still checks whether the exe exists.
        End If

        If wordHandler.Length > 0 Then
            'Sanitize path
            wordHandler = wordHandler.Replace("""", "")
            wordHandler = wordHandler.Replace("@", "")
            wordHandler = wordHandler.Replace("%1", "")


            If wordHandler.ToLower.Contains(".exe") Then
                wordHandler = Trim(Strings.Left(wordHandler, (InStr(wordHandler.ToLower, ".exe") + 3)))
            ElseIf wordHandler.ToLower.Contains(".dll") Then
                wordHandler = Trim(Strings.Left(wordHandler, (InStr(wordHandler.ToLower, ".dll") + 3)))
            End If

            Try
                If IO.File.Exists(wordHandler) Then

                    Try
                        Dim fileDescr As FileVersionInfo
                        fileDescr = FileVersionInfo.GetVersionInfo(wordHandler)

                        If _
                        InStr(fileDescr.ProductVersion.ToString, ".") >= 2 AndAlso IsNumeric(Strings.Left(fileDescr.ProductVersion.ToString, (InStr(fileDescr.ProductVersion.ToString, ".") - 1))) Then
                            _wordVersion = Strings.Left(fileDescr.ProductVersion.ToString, (InStr(fileDescr.ProductVersion.ToString, ".") - 1))

                        ElseIf IsNumeric(fileDescr.ProductVersion.ToString) Then
                            _wordVersion = fileDescr.ProductVersion.ToString

                        ElseIf _
                        InStr(fileDescr.FileVersion.ToString, ".") >= 2 AndAlso IsNumeric(Strings.Left(fileDescr.FileVersion.ToString, (InStr(fileDescr.FileVersion.ToString, ".") - 1))) Then
                            _wordVersion = Strings.Left(fileDescr.FileVersion.ToString, (InStr(fileDescr.FileVersion.ToString, ".") - 1))

                        ElseIf IsNumeric(fileDescr.FileVersion.ToString) Then
                            _wordVersion = fileDescr.FileVersion.ToString

                        End If
                    Catch ex As Exception
                        MsgBox("Can't determine MS Word version." & vbNewLine &
                               "" & vbNewLine &
                               "Reason:" & vbNewLine &
                               ex.Message, MsgBoxStyle.Critical, "Error")
                    End Try

                    _wordIsInstalled = True
                    Exit Sub
                Else
                    _wordIsInstalled = False
                    Exit Sub
                End If
            Catch ex As Exception
                MsgBox("Can't determine if MS Word is installed." & vbNewLine &
                       "" & vbNewLine &
                       "Reason:" & vbNewLine &
                       ex.Message, MsgBoxStyle.Critical, "Error")
                _wordIsInstalled = False
                Exit Sub
            End Try
        Else
            _wordIsInstalled = False
            Exit Sub
        End If

    End Sub

    Public Overloads Sub Dispose()
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub 'Dispose

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
            End If
            ' Dispose managed resources.

            ' Dispose unmanaged resources.
            If Not (wordApp Is Nothing) Then
                Try
                    If isNewApp And wordApp.Documents.Count = 0 Then
                        Dim arg1 As Object = Word.WdSaveOptions.wdDoNotSaveChanges
                        Dim arg2 As Object = Nothing
                        Dim arg3 As Object = Nothing
                        wordApp.Quit(arg1, arg2, arg3)

                        ' Wait until Word shuts down.
                    End If
                Catch
                End Try
                wordApp = Nothing
            End If
        End If
        disposed = True
    End Sub 'Dispose

    Overloads Sub Finalize()
        Dispose(False)
        MyBase.Finalize()
    End Sub 'Finalize

    Public ReadOnly Property Application() As Object
        Get
            Return wordApp
        End Get
    End Property

End Class 'WordLoader