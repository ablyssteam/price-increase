﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.btnExport = New System.Windows.Forms.Button()
        Me.btnProcess = New System.Windows.Forms.Button()
        Me.cboPaymentAmount_new = New System.Windows.Forms.ComboBox()
        Me.cboPaymentBand_new = New System.Windows.Forms.ComboBox()
        Me.cboPaymentType_new = New System.Windows.Forms.ComboBox()
        Me.cboPaymentMonth_new = New System.Windows.Forms.ComboBox()
        Me.cboPaymentAmount = New System.Windows.Forms.ComboBox()
        Me.cboPaymentBand = New System.Windows.Forms.ComboBox()
        Me.cboPaymentType = New System.Windows.Forms.ComboBox()
        Me.cboPaymentMonth = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtInvoiceAddress = New System.Windows.Forms.TextBox()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.cboContacts = New System.Windows.Forms.ComboBox()
        Me.txtCustomerName = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btnPrevious = New System.Windows.Forms.Button()
        Me.lblRecords = New System.Windows.Forms.Label()
        Me.cboMonth = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblDateBecameCustomer = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.rbPurchase = New System.Windows.Forms.RadioButton()
        Me.rbRental = New System.Windows.Forms.RadioButton()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnExport
        '
        Me.btnExport.Image = CType(resources.GetObject("btnExport.Image"), System.Drawing.Image)
        Me.btnExport.Location = New System.Drawing.Point(22, 593)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.Size = New System.Drawing.Size(84, 34)
        Me.btnExport.TabIndex = 59
        Me.btnExport.Text = "Export List"
        Me.btnExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnExport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnProcess
        '
        Me.btnProcess.Image = CType(resources.GetObject("btnProcess.Image"), System.Drawing.Image)
        Me.btnProcess.Location = New System.Drawing.Point(437, 593)
        Me.btnProcess.Name = "btnProcess"
        Me.btnProcess.Size = New System.Drawing.Size(84, 34)
        Me.btnProcess.TabIndex = 58
        Me.btnProcess.Text = "Process"
        Me.btnProcess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnProcess.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnProcess.UseVisualStyleBackColor = True
        '
        'cboPaymentAmount_new
        '
        Me.cboPaymentAmount_new.FormattingEnabled = True
        Me.cboPaymentAmount_new.Location = New System.Drawing.Point(336, 534)
        Me.cboPaymentAmount_new.Name = "cboPaymentAmount_new"
        Me.cboPaymentAmount_new.Size = New System.Drawing.Size(142, 21)
        Me.cboPaymentAmount_new.TabIndex = 57
        '
        'cboPaymentBand_new
        '
        Me.cboPaymentBand_new.FormattingEnabled = True
        Me.cboPaymentBand_new.Items.AddRange(New Object() {"Bronze", "Silver", "Gold"})
        Me.cboPaymentBand_new.Location = New System.Drawing.Point(336, 500)
        Me.cboPaymentBand_new.Name = "cboPaymentBand_new"
        Me.cboPaymentBand_new.Size = New System.Drawing.Size(142, 21)
        Me.cboPaymentBand_new.TabIndex = 56
        '
        'cboPaymentType_new
        '
        Me.cboPaymentType_new.FormattingEnabled = True
        Me.cboPaymentType_new.Items.AddRange(New Object() {"None", "Annual Support", "Rental", "Monthly Support"})
        Me.cboPaymentType_new.Location = New System.Drawing.Point(336, 432)
        Me.cboPaymentType_new.Name = "cboPaymentType_new"
        Me.cboPaymentType_new.Size = New System.Drawing.Size(142, 21)
        Me.cboPaymentType_new.TabIndex = 55
        '
        'cboPaymentMonth_new
        '
        Me.cboPaymentMonth_new.FormattingEnabled = True
        Me.cboPaymentMonth_new.Items.AddRange(New Object() {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"})
        Me.cboPaymentMonth_new.Location = New System.Drawing.Point(336, 466)
        Me.cboPaymentMonth_new.Name = "cboPaymentMonth_new"
        Me.cboPaymentMonth_new.Size = New System.Drawing.Size(142, 21)
        Me.cboPaymentMonth_new.TabIndex = 54
        '
        'cboPaymentAmount
        '
        Me.cboPaymentAmount.FormattingEnabled = True
        Me.cboPaymentAmount.Location = New System.Drawing.Point(131, 533)
        Me.cboPaymentAmount.Name = "cboPaymentAmount"
        Me.cboPaymentAmount.Size = New System.Drawing.Size(142, 21)
        Me.cboPaymentAmount.TabIndex = 53
        '
        'cboPaymentBand
        '
        Me.cboPaymentBand.FormattingEnabled = True
        Me.cboPaymentBand.Items.AddRange(New Object() {"Bronze", "Silver", "Gold"})
        Me.cboPaymentBand.Location = New System.Drawing.Point(131, 499)
        Me.cboPaymentBand.Name = "cboPaymentBand"
        Me.cboPaymentBand.Size = New System.Drawing.Size(142, 21)
        Me.cboPaymentBand.TabIndex = 52
        '
        'cboPaymentType
        '
        Me.cboPaymentType.FormattingEnabled = True
        Me.cboPaymentType.Items.AddRange(New Object() {"None", "Annual Support", "Rental", "Monthly Support"})
        Me.cboPaymentType.Location = New System.Drawing.Point(131, 431)
        Me.cboPaymentType.Name = "cboPaymentType"
        Me.cboPaymentType.Size = New System.Drawing.Size(142, 21)
        Me.cboPaymentType.TabIndex = 51
        '
        'cboPaymentMonth
        '
        Me.cboPaymentMonth.FormattingEnabled = True
        Me.cboPaymentMonth.Items.AddRange(New Object() {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"})
        Me.cboPaymentMonth.Location = New System.Drawing.Point(131, 465)
        Me.cboPaymentMonth.Name = "cboPaymentMonth"
        Me.cboPaymentMonth.Size = New System.Drawing.Size(142, 21)
        Me.cboPaymentMonth.TabIndex = 50
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(28, 537)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(81, 13)
        Me.Label14.TabIndex = 49
        Me.Label14.Text = "Payment Value:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(28, 503)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(90, 13)
        Me.Label13.TabIndex = 48
        Me.Label13.Text = "Support Package"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(29, 469)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(82, 13)
        Me.Label12.TabIndex = 47
        Me.Label12.Text = "Renewal Month"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(28, 435)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(78, 13)
        Me.Label11.TabIndex = 46
        Me.Label11.Text = "Payment Type:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(374, 387)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(57, 25)
        Me.Label10.TabIndex = 45
        Me.Label10.Text = "New"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(146, 387)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(90, 25)
        Me.Label9.TabIndex = 44
        Me.Label9.Text = "Current"
        '
        'Label8
        '
        Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label8.Location = New System.Drawing.Point(31, 360)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(490, 2)
        Me.Label8.TabIndex = 43
        '
        'txtInvoiceAddress
        '
        Me.txtInvoiceAddress.Location = New System.Drawing.Point(174, 236)
        Me.txtInvoiceAddress.Multiline = True
        Me.txtInvoiceAddress.Name = "txtInvoiceAddress"
        Me.txtInvoiceAddress.Size = New System.Drawing.Size(338, 80)
        Me.txtInvoiceAddress.TabIndex = 41
        '
        'txtAddress
        '
        Me.txtAddress.Location = New System.Drawing.Point(174, 142)
        Me.txtAddress.Multiline = True
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(338, 80)
        Me.txtAddress.TabIndex = 40
        '
        'cboContacts
        '
        Me.cboContacts.FormattingEnabled = True
        Me.cboContacts.Location = New System.Drawing.Point(174, 109)
        Me.cboContacts.Name = "cboContacts"
        Me.cboContacts.Size = New System.Drawing.Size(338, 21)
        Me.cboContacts.TabIndex = 39
        '
        'txtCustomerName
        '
        Me.txtCustomerName.Location = New System.Drawing.Point(174, 80)
        Me.txtCustomerName.Name = "txtCustomerName"
        Me.txtCustomerName.Size = New System.Drawing.Size(338, 20)
        Me.txtCustomerName.TabIndex = 38
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(31, 330)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(119, 13)
        Me.Label6.TabIndex = 37
        Me.Label6.Text = "Date Became Customer"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(31, 239)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(83, 13)
        Me.Label5.TabIndex = 36
        Me.Label5.Text = "Invoice Address"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(31, 142)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(92, 13)
        Me.Label4.TabIndex = 35
        Me.Label4.Text = "Customer Address"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(31, 109)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 13)
        Me.Label3.TabIndex = 34
        Me.Label3.Text = "Contacts"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(31, 80)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(82, 13)
        Me.Label2.TabIndex = 33
        Me.Label2.Text = "Customer Name"
        '
        'btnNext
        '
        Me.btnNext.Image = CType(resources.GetObject("btnNext.Image"), System.Drawing.Image)
        Me.btnNext.Location = New System.Drawing.Point(467, 35)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(41, 23)
        Me.btnNext.TabIndex = 4
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnPrevious
        '
        Me.btnPrevious.Image = CType(resources.GetObject("btnPrevious.Image"), System.Drawing.Image)
        Me.btnPrevious.Location = New System.Drawing.Point(420, 35)
        Me.btnPrevious.Name = "btnPrevious"
        Me.btnPrevious.Size = New System.Drawing.Size(41, 23)
        Me.btnPrevious.TabIndex = 3
        Me.btnPrevious.UseVisualStyleBackColor = True
        '
        'lblRecords
        '
        Me.lblRecords.AutoSize = True
        Me.lblRecords.Location = New System.Drawing.Point(319, 40)
        Me.lblRecords.Name = "lblRecords"
        Me.lblRecords.Size = New System.Drawing.Size(75, 13)
        Me.lblRecords.TabIndex = 2
        Me.lblRecords.Text = "Record 1 o f 1"
        '
        'cboMonth
        '
        Me.cboMonth.FormattingEnabled = True
        Me.cboMonth.Items.AddRange(New Object() {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"})
        Me.cboMonth.Location = New System.Drawing.Point(113, 36)
        Me.cboMonth.Name = "cboMonth"
        Me.cboMonth.Size = New System.Drawing.Size(171, 21)
        Me.cboMonth.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(37, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Select Month"
        '
        'lblDateBecameCustomer
        '
        Me.lblDateBecameCustomer.AutoSize = True
        Me.lblDateBecameCustomer.Location = New System.Drawing.Point(171, 330)
        Me.lblDateBecameCustomer.Name = "lblDateBecameCustomer"
        Me.lblDateBecameCustomer.Size = New System.Drawing.Size(65, 13)
        Me.lblDateBecameCustomer.TabIndex = 42
        Me.lblDateBecameCustomer.Text = "01/01/2020"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Gray
        Me.Panel1.Controls.Add(Me.rbRental)
        Me.Panel1.Controls.Add(Me.rbPurchase)
        Me.Panel1.Controls.Add(Me.btnNext)
        Me.Panel1.Controls.Add(Me.btnPrevious)
        Me.Panel1.Controls.Add(Me.lblRecords)
        Me.Panel1.Controls.Add(Me.cboMonth)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(538, 74)
        Me.Panel1.TabIndex = 32
        '
        'Label7
        '
        Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label7.Location = New System.Drawing.Point(25, 573)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(490, 2)
        Me.Label7.TabIndex = 60
        '
        'rbPurchase
        '
        Me.rbPurchase.AutoSize = True
        Me.rbPurchase.Checked = True
        Me.rbPurchase.Location = New System.Drawing.Point(40, 12)
        Me.rbPurchase.Name = "rbPurchase"
        Me.rbPurchase.Size = New System.Drawing.Size(143, 17)
        Me.rbPurchase.TabIndex = 5
        Me.rbPurchase.TabStop = True
        Me.rbPurchase.Text = "Purchase && New Rentals"
        Me.rbPurchase.UseVisualStyleBackColor = True
        '
        'rbRental
        '
        Me.rbRental.AutoSize = True
        Me.rbRental.Location = New System.Drawing.Point(199, 13)
        Me.rbRental.Name = "rbRental"
        Me.rbRental.Size = New System.Drawing.Size(116, 17)
        Me.rbRental.TabIndex = 6
        Me.rbRental.Text = "Rental Over 1 Year"
        Me.rbRental.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(538, 639)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.btnExport)
        Me.Controls.Add(Me.btnProcess)
        Me.Controls.Add(Me.cboPaymentAmount_new)
        Me.Controls.Add(Me.cboPaymentBand_new)
        Me.Controls.Add(Me.cboPaymentType_new)
        Me.Controls.Add(Me.cboPaymentMonth_new)
        Me.Controls.Add(Me.cboPaymentAmount)
        Me.Controls.Add(Me.cboPaymentBand)
        Me.Controls.Add(Me.cboPaymentType)
        Me.Controls.Add(Me.cboPaymentMonth)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtInvoiceAddress)
        Me.Controls.Add(Me.txtAddress)
        Me.Controls.Add(Me.cboContacts)
        Me.Controls.Add(Me.txtCustomerName)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblDateBecameCustomer)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnExport As Button
    Friend WithEvents btnProcess As Button
    Friend WithEvents cboPaymentAmount_new As ComboBox
    Friend WithEvents cboPaymentBand_new As ComboBox
    Friend WithEvents cboPaymentType_new As ComboBox
    Friend WithEvents cboPaymentMonth_new As ComboBox
    Friend WithEvents cboPaymentAmount As ComboBox
    Friend WithEvents cboPaymentBand As ComboBox
    Friend WithEvents cboPaymentType As ComboBox
    Friend WithEvents cboPaymentMonth As ComboBox
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents txtInvoiceAddress As TextBox
    Friend WithEvents txtAddress As TextBox
    Friend WithEvents cboContacts As ComboBox
    Friend WithEvents txtCustomerName As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents btnNext As Button
    Friend WithEvents btnPrevious As Button
    Friend WithEvents lblRecords As Label
    Friend WithEvents cboMonth As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents lblDateBecameCustomer As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents rbRental As RadioButton
    Friend WithEvents rbPurchase As RadioButton
    Friend WithEvents Label7 As Label
End Class
